const listUser = [{
        id: 1,
        name: "Kelvin",
        lastname: "Carrión A.",
    },
    {
        id: 20,
        name: "Milagros",
        lastname: "Condori Q.",
    },
];

class User {
    list() {
        return listUser;
    }

    show(userId) {
        return new Promise((resolve, reject) => {
            const userEncontrado = listUser.find((user) => user.id === userId);
            if (userEncontrado) {
                return resolve(userEncontrado);
            } else {
                return reject({ message: "UserId no encontrado" });
            }
        });
    }

    create(id, name, lastname) {
        const newUser = {
            id: id,
            name: name,
            lastname: lastname,
        }
        listUser.push(newUser);
        return newUser
    }

    update(id, name, lastname) {
        return new Promise((resolve, reject) => {
            const userEncontrado = listUser.find((user) => user.id === id);
            if (userEncontrado) {
                userEncontrado.name = name,
                    userEncontrado.lastname = lastname


                return resolve(userEncontrado);
            } else {
                return reject({ message: "UserId no encontrado" });
            }
        })


    }

    delete(id) {
        return new Promise((resolve, reject) => {
            const userEncontrado = listUser.find((u) => u.id === id)
            if (userEncontrado) {

                const index = listUser.findIndex((u) => u.id === id)
                const indexD = listUser.indexOf(userEncontrado)

                listUser.splice(indexD, 1)


                return resolve(userEncontrado)
            } else {
                return reject({ messaje: "UserId no encontrado" });
            }
        })
    }

}

module.exports = new User();